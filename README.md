![GitLab loves Open Source](images/gitlab-loves-open-source.png)

# GitLab top tiers are free for Open Source Projects

GitLab exists today in large part thanks to the work of a vast community of open source contributors around the world. As a company, we are deeply committed to being a good [steward for the Open Source project](https://about.gitlab.com/stewardship). To give back to this community who gives us so much, we want to enable teams be more efficient, secure, and productive. We believe the best way for them to achieve this is by using as many of the capabilities of GitLab as possible.

As such, we are offering a complimentary license to GitLab Ultimate (self-managed or SaaS) to any Open Source project to qualifying open source projects. 

See requirements and how to apply here: https://about.gitlab.com/solutions/open-source/join/

## Benefits

- Free Ultimate tier (self-managed or SaaS)
- 50,000 free CI min/month
- Optional paid support at a 95% discount ($4.95/user/month instead of $99/user/month).


## License and subscription details

Please see our [GitLab for Open Source terms and conditions](https://about.gitlab.com/handbook/legal/opensource-agreement/).
